#!/bin/bash

mkdir -p build/release

cd build/release

cmake -DCMAKE_BUILD_TYPE=release -DCMAKE_EXPORT_COMPILE_COMMANDS=ON ../..

# [unusedFunction] forbids to build a library
# [missingIncludeSystem] not applicable as cppcheck cannot find system includes
# [unmatchedSuppression] displays unmatched suppressions
# [missingInclude] forbids to exclude googletest sources

cppcheck --version

cppcheck --project=compile_commands.json --error-exitcode=-1 --enable=all --std=c++11 \
--suppress=unusedFunction \
--suppress=missingIncludeSystem \
--suppress=unmatchedSuppression \
--suppress=missingInclude \
"$@"
