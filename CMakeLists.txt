cmake_minimum_required(VERSION 3.13)

include(osef.cmake)

project(osef-modbus)

add_subdirectory(osef-netbuffer)
add_subdirectory(osef-netbuffer/osef-posix/Mutex)

add_library(${PROJECT_NAME} STATIC)

target_sources(${PROJECT_NAME}
    PRIVATE
        Modbus/ModbusApplication.cpp
        Modbus/ModbusBoolContainer.cpp
        Modbus/ModbusBuffer.cpp
        Modbus/ModbusClient.cpp
        Modbus/ModbusRegContainer.cpp
        Modbus/ModbusRequestReadCoils.cpp
        Modbus/ModbusRequestReadHoldingReg.cpp
        Modbus/ModbusRequestReadInputReg.cpp
        Modbus/ModbusRequestWriteCoils.cpp
        Modbus/ModbusRequestWriteHoldingReg.cpp
        Modbus/ModbusResponseReadCoils.cpp
        Modbus/ModbusResponseReadHoldingReg.cpp
        Modbus/ModbusResponseReadInputReg.cpp
        Modbus/ModbusResponseWriteCoils.cpp
        Modbus/ModbusResponseWriteHoldingReg.cpp
        Modbus/ModbusServer.cpp
)

target_include_directories(${PROJECT_NAME}
    PUBLIC
        Modbus
)

target_link_libraries(${PROJECT_NAME}
    osef-netbuffer
    osef-mutex
)
