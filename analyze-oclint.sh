#!/bin/bash

mkdir -p build/release

cd build/release

cmake -DCMAKE_BUILD_TYPE=release -DCMAKE_EXPORT_COMPILE_COMMANDS=ON ../..

#[design|P3] AvoidPrivateStaticMembers forbids private directory for queues
#[size|P3] LongLine forbids lines longer than 100 characters
#[size|P3] DeepNestedBlock forbids nested depth beyond 5
#[convention|P3] InvertedLogic forbids if ( ptr != nullptr)
#[convention|P3] ParameterReassignment forbids to retrieve value through parameter
#[convention|P3] PreferEarlyExit
#[naming|P3] ShortVariableName

oclint --version

oclint "$@" \
-disable-rule=LongLine \
-disable-rule=DeepNestedBlock \
-disable-rule=ParameterReassignment \
-disable-rule=PreferEarlyExit \
-disable-rule=InvertedLogic \
-disable-rule=AvoidPrivateStaticMembers \
-disable-rule=ShortVariableName \
