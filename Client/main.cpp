#include "SocketToolbox.h"
#include "ModbusClient.h"
#include "Randomizer.h"
#include "Debug.h"

#include <getopt.h>

typedef struct
{
    int32_t argc;
    char** argv;
}Arguments;

bool getHostService(const Arguments& arg, std::string& host, std::string& port)
{
    bool ret = true;

    if (arg.argc > 1)
    {
        /*use function getopt to get the arguments with option."hu:p:s:v" indicate
        that option h,v are the options without arguments while u,p,s are the
        options with arguments*/
        int32_t opt = -1;
        do
        {
            opt = getopt(arg.argc, arg.argv, "i:p:");
            if (opt != -1)
            {
                switch (opt)
                {
                    case 'i':
                        host = optarg;
                        break;
                    case 'p':
                        port = optarg;
                        break;
                    default:
                        std::cout << "Usage:   " << arg.argv[0UL] << " [-option] [argument]" << std::endl;
                        std::cout << "option:  " << std::endl;
                        std::cout << "         " << "-i  Ethernet IP host (localhost/127.0.0.1)" << std::endl;
                        std::cout << "         " << "-p  Service port" << std::endl;
                        ret = false;
                        break;
                }
            }
        }while (opt != -1);
    }

    return ret;
}

void getRandomCoils(uint16_t& adr, std::vector<bool>& read, std::vector<bool>& write)
{
    adr = OSEF::Randomizer().getUInt16();

    size_t nCoils = OSEF::Randomizer().getUInt16()%1969;
    if (nCoils == 0)
    {
        nCoils = 1;
    }

    read.insert(read.begin(), nCoils, false);

    for (size_t i = 0; i < nCoils; i++)
    {
        if (OSEF::Randomizer().getUInt32()%2)
        {
            write.push_back(true);
        }
        else
        {
            write.push_back(false);
        }
    }
}

bool testMultipleCoils(OSEF::ModbusClient& client)
{
    bool ret = false;

    uint16_t adr;
    std::vector<bool> read;
    std::vector<bool> write;
    getRandomCoils(adr, read, write);
    timespec to = {1, 0};

    if (client.readCoils(adr, read, to))
    {
        DOUT("read " << read.size() << " coils from #" << adr);

        if (client.writeCoils(adr, write, to))
        {
            DOUT("write " << write.size() << " coils from #" << adr);

            if (client.readCoils(adr, read, to))
            {
                DOUT("read again " << read.size() << " coils from #" << adr);

                for (size_t i = 0; i < read.size(); i++)
                {
                    if (read[i] == write[i])
                    {
                        ret = true;
//                        DOUT("read again coils #" << adr+i << " = " << read[i] << " equals written value " << write[i]);
                    }
                    else
                    {
                        DERR("read again coils #" << adr+i << " = " << read[i] << " different from written value " << write[i]);
                    }
                }
            }
            else
            {
                DERR("error second reading coils" << adr);
            }
        }
        else
        {
            DERR("error writing coils" << adr);
        }
    }
    else
    {
        DERR("error first reading coils" << adr);
    }

    return ret;
}

bool testSingleHoldingRegister(OSEF::ModbusClient& client)
{
    bool ret = false;

    uint16_t adr = OSEF::Randomizer().getUInt16();
    uint16_t write = OSEF::Randomizer().getUInt16();

    uint16_t read = 0;
    timespec to = {1, 0};
    if (client.readHoldingReg(adr, read, to))
    {
        DOUT("read holding register #" << adr << " = " << read);
        if (client.writeHoldingReg(adr, write, to))
        {
            DOUT("write holding register #" << adr << " = " << write);
            if (client.readHoldingReg(adr, read, to))
            {
                if (read == write)
                {
                    ret = true;
                    DOUT("read again holding register #" << adr << " = " << read << " equals written value " << write);
                }
                else
                {
                    DERR("read again holding register #" << adr << " = " << read << " different from written value " << write);
                }
            }
            else
            {
                DERR("error second reading holding register" << adr);
            }
        }
        else
        {
            DERR("error writing holding register" << adr);
        }
    }
    else
    {
        DERR("error first reading holding register" << adr);
    }

    return ret;
}

bool testSingleInputReg(OSEF::ModbusClient& client)
{
    bool ret = false;

    uint16_t adr = OSEF::Randomizer().getUInt16();
    uint16_t qty = OSEF::Randomizer().getUInt16();

    qty %= 126;
    if (qty == 0)
    {
        qty = 1;
    }

    std::vector<uint16_t> read;
    read.insert(read.begin(), qty, 0xffff);

    timespec to = {1, 0};
    if (client.readInputReg(adr, qty, read, to))
    {
        ret = true;

        for (size_t i = 0; i < qty; i++)
        {
            if (read[i] != 0)
            {
                DERR("read input register #" << adr+i << " = " << read[i] << " different from 0 ");
                ret = false;
            }
        }

        if (ret)
        {
            DOUT("read " << qty << " input registers from #" << adr);
        }
    }
    else
    {
        DERR("error reading input register " << adr);
    }

    return ret;
}

int main(int argc, char** argv)
{
    int ret = -1;

    std::string host = "localhost";
    std::string port = "1502";
    if (getHostService({argc, argv}, host, port))
    {
        OSEF::ModbusClient client(host, port);

        if (testMultipleCoils(client))
        {
            if (testSingleHoldingRegister(client))
            {
                if (testSingleInputReg(client))
                {
                    ret = 0;
                }
            }
        }
    }

    return ret;
}
