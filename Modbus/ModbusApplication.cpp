#include "ModbusApplication.h"
#include "NetBufferSession.h"

OSEF::ModbusApplication::ModbusApplication(const uint16_t& ncoils, const uint16_t& ninputs, const uint16_t& nhregs, const uint16_t& niregs)
    :modbusServer(ncoils, ninputs, nhregs, niregs) {}

bool OSEF::ModbusApplication::serve(TcpServer& server)
{
    OSEF::NetBufferSession session(&server);
    OSEF::ModbusBuffer request;
    OSEF::ModbusBuffer response;
    bool connectionOpen = true;
    bool serviceOk = true;
    do
    {
        if (session.receiveBuffer(request))
        {
            serviceOk = false;
            if (modbusServer.serveRequest(request, response))
            {
                if (session.sendBuffer(response))
                {
                    serviceOk = true;
                }
            }
        }
        else
        {
            connectionOpen = false;
        }
    }while (connectionOpen && serviceOk);

    return serviceOk;
}
