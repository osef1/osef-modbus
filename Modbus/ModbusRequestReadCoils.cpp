#include "ModbusRequestReadCoils.h"
#include "Debug.h"

OSEF::ModbusRequestReadCoils::ModbusRequestReadCoils(const uint16_t& adr, const uint16_t& nCoils)
    :ModbusBuffer(2)
{
    if (setFunctionCode(1))
    {
        if (setAddress(adr))
        {
            if (not pushUInt16(nCoils))
            {
                DERR("error initializing coils quantity");
            }
        }
        else
        {
            DERR("error initializing first coil address");
        }
    }
    else
    {
        DERR("error initializing read coils function code");
    }
}

OSEF::ModbusRequestReadCoils::~ModbusRequestReadCoils() = default;
