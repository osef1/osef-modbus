#ifndef DCMODBUSREQUESTREADCOILS_H
#define DCMODBUSREQUESTREADCOILS_H

#include "ModbusBuffer.h"

namespace OSEF
{
    class ModbusRequestReadCoils : public ModbusBuffer
    {
    public:
        explicit ModbusRequestReadCoils(const uint16_t& adr, const uint16_t& nCoils = 1);
        ~ModbusRequestReadCoils() override;

        ModbusRequestReadCoils(const ModbusRequestReadCoils&) = delete;  // copy constructor
        ModbusRequestReadCoils& operator=(const ModbusRequestReadCoils&) = delete;  // copy assignment
        ModbusRequestReadCoils(ModbusRequestReadCoils&&) = delete;  // move constructor
        ModbusRequestReadCoils& operator=(ModbusRequestReadCoils&&) = delete;  // move assignment
    };
}  // namespace OSEF

#endif /* DCMODBUSREQUESTREADCOILS_H */
