#include "ModbusRegContainer.h"
#include "Debug.h"

OSEF::ModbusRegContainer::ModbusRegContainer(const size_t& nregs)
    :regs(nregs, 0) {}

OSEF::ModbusRegContainer::~ModbusRegContainer() = default;

bool OSEF::ModbusRegContainer::check(const size_t& adr) const
{
    bool ret = false;

    if (static_cast<size_t>(adr) < regs.size())
    {
        ret = true;
    }

    return ret;
}

bool OSEF::ModbusRegContainer::get(const size_t& adr, uint16_t& val) const
{
    bool ret = false;

    if (check(adr))
    {
        val = regs[adr];
        ret = true;
    }

    return ret;
}

bool OSEF::ModbusRegContainer::set(const size_t& adr, const uint16_t& val)
{
    bool ret = false;

    if (check(adr))
    {
        regs[adr] = val;
        ret = true;
    }

    return ret;
}
