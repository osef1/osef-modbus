#ifndef DCMODBUSRESPONSEREADINPUTREG_H
#define DCMODBUSRESPONSEREADINPUTREG_H

#include "ModbusRequestReadInputReg.h"
#include <vector>

namespace OSEF
{
    class ModbusResponseReadInputReg : public ModbusBuffer
    {
    public:
        ModbusResponseReadInputReg() = default;
        ~ModbusResponseReadInputReg() override = default;

        bool checkResponse(ModbusRequestReadInputReg& request, uint16_t& reg);  // check buffer and extract read register
        bool checkResponse(ModbusRequestReadInputReg& request, std::vector<uint16_t>& regs);  // check buffer and extract read registers

        ModbusResponseReadInputReg(const ModbusResponseReadInputReg&) = delete;  // copy constructor
        ModbusResponseReadInputReg& operator=(const ModbusResponseReadInputReg&) = delete;  // copy assignment
        ModbusResponseReadInputReg(ModbusResponseReadInputReg&&) = delete;  // move constructor
        ModbusResponseReadInputReg& operator=(ModbusResponseReadInputReg&&) = delete;  // move assignment
    };
}  // namespace OSEF

#endif /* DCMODBUSRESPONSEREADINPUTREG_H */
