#include "ModbusBuffer.h"
#include "Debug.h"  // DERR

// ADU = 7 bytes MBAP + 253 bytes PDU, offset = 7 bytes MBAP + 1 byte function code
OSEF::ModbusBuffer::ModbusBuffer(const size_t& addOffset)
    :NetBuffer(260U, 8+addOffset) {}

bool OSEF::ModbusBuffer::resetToResponse(ModbusBuffer& req, const uint8_t& fc)
{
    bool ret = false;

    uint16_t trid = 0;
    if (req.getTransactionId(trid))
    {
        if (NetBuffer::resetForSetup())  // addOffset))
        {
            if (setTransactionId(trid))
            {
                if (setProtocolId(0))
                {
                    if (setUnitId(0))
                    {
                        ret = setFunctionCode(fc);
                    }
                }
            }
        }
    }

    return ret;
}

bool OSEF::ModbusBuffer::resetToException(ModbusBuffer& req, const uint8_t& fc, const uint8_t& exc)
{
    bool ret = false;

    uint16_t trid = 0;
    if (req.getTransactionId(trid))
    {
        if (NetBuffer::resetForSetup())
        {
            if (setTransactionId(trid))
            {
                if (setProtocolId(0))
                {
                    if (setUnitId(0))
                    {
                        ret = pushException(fc, exc);
                    }
                }
            }
        }
    }

    return ret;
}

bool OSEF::ModbusBuffer::setLimits(const size_t& dl, const size_t& offset, const size_t& offsetmin)
{
    bool ret = false;

    if (NetBuffer::setLimits(dl, offset, offsetmin))
    {
        ret = setLength(dl);
    }

    return ret;
}

bool OSEF::ModbusBuffer::setTransactionId(const uint16_t& trid)
{
    const bool ret = setUInt16(trid, 0);  // transaction id
    return ret;
}

bool OSEF::ModbusBuffer::setProtocolId(const uint16_t& prid)
{
    const bool ret = setUInt16(prid, 2);  // protocol id
    return ret;
}

bool OSEF::ModbusBuffer::setLength(const uint16_t& length)
{
    bool ret = true;

    if (length>static_cast<uint16_t>(6))
    {
        ret = setUInt16(length-6, 4);  // length including Unit id
    }

    return ret;
}

bool OSEF::ModbusBuffer::setUnitId(const uint8_t& unid)
{
    const bool ret = setUInt8(unid, 6);  // unit id
    return ret;
}

bool OSEF::ModbusBuffer::setFunctionCode(const uint8_t& fc)
{
    const bool ret = setUInt8(fc, 7);
    return ret;
}

bool OSEF::ModbusBuffer::setAddress(const uint16_t& adr)
{
    const bool ret = setUInt16(adr, 8);
    return ret;
}

bool OSEF::ModbusBuffer::setValue(const uint16_t& val)
{
    const bool ret = setUInt16(val, 10);
    return ret;
}

bool OSEF::ModbusBuffer::setQuantity(const uint16_t& q)
{
    const bool ret = setUInt16(q, 10);
    return ret;
}

bool OSEF::ModbusBuffer::setAndMask(const uint16_t& m)
{
    const bool ret = setUInt16(m, 10);
    return ret;
}

bool OSEF::ModbusBuffer::setOrMask(const uint16_t& m)
{
    const bool ret = setUInt16(m, 12);
    return ret;
}

bool OSEF::ModbusBuffer::pushException(const uint8_t& fc, const uint8_t& exc)
{
    bool ret = false;

    if (setFunctionCode(fc+0x80))
    {
        ret = pushUInt8(exc);
    }

    return ret;
}

bool OSEF::ModbusBuffer::pushDeviceIdentificationObject(const uint8_t& oi, const std::string& obj)
{
    bool ret = false;

    if (getRemainingSizeToPush() >= (obj.size()+2U))
    {
        if (pushUInt8(oi))  // object id
        {
            if (pushUInt8(obj.size()))  // object length
            {
                ret = pushString(obj);  // object value
            }
        }
    }

    return ret;
}

uint16_t OSEF::ModbusBuffer::getSizeInBytes(const uint16_t& nBits) const
{
    uint16_t ret = nBits/static_cast<uint16_t>(8);

    if ((nBits%8) > 0)
    {
        ret++;
    }

    return ret;
}

size_t OSEF::ModbusBuffer::pushRegs(const ModbusRegContainer& regs, const size_t& adr, const size_t& n)
{
    bool pushed = true;
    uint16_t reg = 0;
    size_t i = 0;
    while ( (i < n) && pushed )
    {
        if (regs.get(adr+i, reg))
        {
            DOUT("pushing reg #" << adr+i << " = " << reg);

            if (pushUInt16(reg))
            {
                i++;
            }
            else
            {
                pushed = false;
            }
        }
        else
        {
            pushed = false;
        }
    }

    return i;
}

size_t OSEF::ModbusBuffer::pushBoolsBytes(const ModbusBoolContainer& bools, const size_t& adr, const size_t& n)
{
    const size_t nBytes = getSizeInBytes(n);
    uint8_t byte = 0;
    size_t i = 0;
    bool keepgoing = true;

    while ((i < nBytes) && keepgoing)
    {
        uint8_t nBits = 8U;
        if ( (n-(i*8UL)) < 8UL )
        {
            nBits = static_cast<uint8_t>(n-(i*8U));
        }

        if (bools.getByte(adr+(i*8U), nBits, byte))
        {
            DOUT("pushing bits #" << adr+(i*8U) << " = 0x" << std::hex << +byte << std::dec);

            if (pushUInt8(byte))
            {
                i++;
            }
            else
            {
                keepgoing = false;
            }
        }
        else
        {
            keepgoing = false;
        }
    }

    return i;
}

bool OSEF::ModbusBuffer::getTransactionId(uint16_t& trid)
{
    const bool ret = getUInt16(trid, 0);
    return ret;
}

bool OSEF::ModbusBuffer::getProtocolId(uint16_t& prid)
{
    const bool ret = getUInt16(prid, 2);
    return ret;
}

bool OSEF::ModbusBuffer::getLength(uint16_t& length)
{
    const bool ret = getUInt16(length, 4);
    return ret;
}

bool OSEF::ModbusBuffer::getUnitId(uint8_t& unid)
{
    const bool ret = getUInt8(unid, 6);
    return ret;
}

bool OSEF::ModbusBuffer::getFunctionCode(uint8_t& fc)
{
    const bool ret = getUInt8(fc, 7);
    return ret;
}

bool OSEF::ModbusBuffer::getAddress(uint16_t& adr)
{
    const bool ret = getUInt16(adr, 8);
    return ret;
}

bool OSEF::ModbusBuffer::getValue(uint16_t& val)
{
    const bool ret = getUInt16(val, 10);
    return ret;
}

bool OSEF::ModbusBuffer::getQuantity(uint16_t& q)
{
    const bool ret = getUInt16(q, 10);
    return ret;
}

bool OSEF::ModbusBuffer::getAndMask(uint16_t& m)
{
    const bool ret = getUInt16(m, 10);
    return ret;
}

bool OSEF::ModbusBuffer::getOrMask(uint16_t& m)
{
    const bool ret = getUInt16(m, 12);
    return ret;
}

bool OSEF::ModbusBuffer::setEncapsulatedInterface(const uint8_t& mei)
{
    const bool ret = setUInt8(mei, 8);
    return ret;
}

bool OSEF::ModbusBuffer::setReadDeviceIdCode(const uint8_t& rc)
{
    const bool ret = setUInt8(rc, 9);
    return ret;
}

bool OSEF::ModbusBuffer::setObjectId(const uint8_t& oi)
{
    const bool ret = setUInt8(oi, 10);
    return ret;
}

bool OSEF::ModbusBuffer::setConformityLevel(const uint8_t& cl)
{
    const bool ret = setUInt8(cl, 10);
    return ret;
}

bool OSEF::ModbusBuffer::setMorefollows(const bool& mf)
{
    bool ret = false;

    if (mf)
    {
        ret = setUInt8(0xff, 11);
    }
    else
    {
        ret = setUInt8(0, 11);
    }

    return ret;
}

bool OSEF::ModbusBuffer::setNextObjectId(const uint8_t& ni)
{
    const bool ret = setUInt8(ni, 12);
    return ret;
}

bool OSEF::ModbusBuffer::setNumberOfObjects(const uint8_t& no)
{
    const bool ret = setUInt8(no, 13);
    return ret;
}

bool OSEF::ModbusBuffer::getEncapsulatedInterface(uint8_t& mei)
{
    const bool ret = getUInt8(mei, 8);
    return ret;
}

bool OSEF::ModbusBuffer::getReadDeviceIdCode(uint8_t& rc)
{
    const bool ret = getUInt8(rc, 9);
    return ret;
}

bool OSEF::ModbusBuffer::getObjectId(uint8_t& oi)
{
    const bool ret = getUInt8(oi, 10);
    return ret;
}

bool OSEF::ModbusBuffer::getConformityLevel(uint8_t& cl)
{
    const bool ret = getUInt8(cl, 10);
    return ret;
}

bool OSEF::ModbusBuffer::getMorefollows(bool& mf)
{
    bool ret = false;

    uint8_t mfbyte = 0;
    if (getUInt8(mfbyte, 11))
    {
        mf = mfbyte != 0;

        ret = true;
    }

    return ret;
}

bool OSEF::ModbusBuffer::getNextObjectId(uint8_t& ni)
{
    const bool ret = getUInt8(ni, 12);
    return ret;
}

bool OSEF::ModbusBuffer::getNumberOfObjects(uint8_t& no)
{
    const bool ret = getUInt8(no, 13);
    return ret;
}

bool OSEF::ModbusBuffer::checkResponse(ModbusBuffer& request)
{
    bool ret = false;

    if (isReadable())
    {
        uint16_t reqtrid = 0;
        uint16_t rsptrid = 0;
        if (request.getTransactionId(reqtrid) && getTransactionId(rsptrid))
        {
            if (reqtrid == rsptrid)
            {
                uint8_t reqfc = 0;
                uint8_t rspfc = 0;
                if (request.getFunctionCode(reqfc) && getFunctionCode(rspfc))
                {
                    if ( reqfc == rspfc)  // (rspfc & 0x7FU) )
                    {
                        ret = true;
                    }
                    else
                    {
                        DERR("wrong function code " << static_cast<uint32_t>(rspfc) << "!=" << static_cast<uint32_t>(reqfc));
                    }
                }
            }
            else
            {
                DERR("wrong transaction id " << rsptrid << "!=" << reqtrid);
            }
        }
    }
    else
    {
        DERR("not enough data to check");
    }

    return ret;
}
