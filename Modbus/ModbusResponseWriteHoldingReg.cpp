#include "ModbusResponseWriteHoldingReg.h"

OSEF::ModbusResponseWriteHoldingReg::ModbusResponseWriteHoldingReg()
    :ModbusBuffer(4) {}

bool OSEF::ModbusResponseWriteHoldingReg::checkResponse(ModbusRequestWriteHoldingReg& request)
{
    bool ret = false;

    if (ModbusBuffer::checkResponse(request))
    {
        if (checkAddress(request))
        {
            ret = checkValue(request);
        }
    }

    return ret;
}

bool OSEF::ModbusResponseWriteHoldingReg::checkAddress(ModbusRequestWriteHoldingReg& request)
{
    bool ret = false;

    uint16_t reqadr = 0;
    uint16_t rspadr = 0;
    if (request.getAddress(reqadr) && getAddress(rspadr))
    {
        if (reqadr == rspadr)
        {
            ret = true;
        }
    }

    return ret;
}

bool OSEF::ModbusResponseWriteHoldingReg::checkValue(ModbusRequestWriteHoldingReg& request)
{
    bool ret = false;

    uint16_t reqval = 0;
    uint16_t rspval = 0;
    if (request.getValue(reqval) && getValue(rspval))
    {
        if (reqval == rspval)
        {
            ret = true;
        }
    }

    return ret;
}
