#include "ModbusBoolContainer.h"
#include "Debug.h"

OSEF::ModbusBoolContainer::ModbusBoolContainer(const size_t& nbools)
    :bools(nbools, {false}) {}

OSEF::ModbusBoolContainer::~ModbusBoolContainer() = default;

bool OSEF::ModbusBoolContainer::check(const size_t& adr) const
{
    bool ret = false;

    if (static_cast<size_t>(adr) < bools.size())
    {
        ret = true;
    }

    return ret;
}

bool OSEF::ModbusBoolContainer::get(const size_t& adr, bool& val) const
{
    bool ret = false;

    if (check(adr))
    {
        val = bools[adr];
        ret = true;
    }

    return ret;
}

bool OSEF::ModbusBoolContainer::getByte(const size_t& adr, const size_t& n, uint8_t& byte) const
{
    bool ret = false;

    if ((n > 0) && (n <= 8UL))
    {
        if (check(adr+(n-1)))
        {
            byte = 0;
            for (size_t i = 0; i < n; i++)
            {
                if (bools[adr+i])
                {
                    byte |= static_cast<uint8_t>(1U << i);
                }
            }

            ret = true;
        }
    }

    return ret;
}

bool OSEF::ModbusBoolContainer::set(const size_t& adr, const bool& val)
{
    bool ret = false;

    if (check(adr))
    {
        bools[adr] = val;
        ret = true;
    }

    return ret;
}

bool OSEF::ModbusBoolContainer::set(const size_t& adr, const uint8_t& val, const uint8_t& size)
{
    bool ret = true;

    uint8_t i = 0;
    while (ret && (i<size) && (i<static_cast<uint8_t>(8)))
    {
        ret = set(adr+i, (val&static_cast<uint8_t>(1U << i)) != 0);
        i++;
    }

    return ret;
}

bool OSEF::ModbusBoolContainer::set(const size_t& adr)
{
    const bool ret = set(adr, true);
    return ret;
}

bool OSEF::ModbusBoolContainer::reset(const size_t& adr)
{
    const bool ret = set(adr, false);
    return ret;
}
