#include "ModbusRequestReadHoldingReg.h"
#include "Debug.h"

OSEF::ModbusRequestReadHoldingReg::ModbusRequestReadHoldingReg(const uint16_t& adr)
    :ModbusBuffer(4)
{
    if (setFunctionCode(3))
    {
        if (setAddress(adr))
        {
            if (not setQuantity(1))  // quantity
            {
                DERR("error initializing input register quantity");
            }
        }
        else
        {
            DERR("error initializing input register address");
        }
    }
    else
    {
        DERR("error initializing read input register function code");
    }
}

OSEF::ModbusRequestReadHoldingReg::~ModbusRequestReadHoldingReg() = default;
