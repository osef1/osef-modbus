#include "ModbusRequestWriteCoils.h"
#include "Debug.h"

OSEF::ModbusRequestWriteCoils::ModbusRequestWriteCoils(const uint16_t& adr, const std::vector<bool>& coils)
    :ModbusBuffer(4)
{
    if (setFunctionCode(15))
    {
        if (setAddress(adr))
        {
            if (setQuantity(coils.size()))
            {
                auto nBytes = static_cast<uint8_t>(getSizeInBytes(coils.size()));
                if (pushUInt8(nBytes))
                {
                    bool ret = false;
                    size_t b = 0;
                    size_t c = 0;
                    do
                    {
                        uint8_t byte = 0;
                        for (uint8_t i = 0; (i < 8) && (c < coils.size()); i++, c++)
                        {
                            if (coils[c])
                            {
                                byte |= 1 << i;
                            }
                        }
                        b++;

                        ret = pushUInt8(byte);
                    }while (ret && (b < nBytes) && (c < coils.size()));

                    if (not ret)
                    {
                        DERR("error pushing coils bytes");
                    }
                }
                else
                {
                    DERR("error initializing bytes quantity");
                }
            }
            else
            {
                DERR("error initializing coils quantity");
            }
        }
        else
        {
            DERR("error initializing first coil address");
        }
    }
    else
    {
        DERR("error initializing write coils function code");
    }
}

OSEF::ModbusRequestWriteCoils::~ModbusRequestWriteCoils() = default;
