#ifndef DCMODBUSCLIENT_H
#define DCMODBUSCLIENT_H

#include "TcpClient.h"
#include "NetBufferSession.h"
#include "ModbusBuffer.h"

#include <string>
#include <vector>

namespace OSEF
{
    class ModbusClient
    {
    public:
        ModbusClient(const std::string& h, const std::string& p);
        virtual ~ModbusClient();

        bool readCoils(const uint16_t& adr, std::vector<bool>& coils, const timespec& to);
        bool writeCoils(const uint16_t& adr, const std::vector<bool>& coils, const timespec& to);

        bool readHoldingReg(const uint16_t& adr, uint16_t& reg, const timespec& to);
        bool readInputReg(const uint16_t& adr, const uint16_t& qty, std::vector<uint16_t>& regs, const timespec& to);
        bool writeHoldingReg(const uint16_t& adr, const uint16_t& val, const timespec& to);

        ModbusClient(const ModbusClient&) = delete;  // copy constructor
        ModbusClient& operator=(const ModbusClient&) = delete;  // copy assignment
        ModbusClient(ModbusClient&&) = delete;  // move constructor
        ModbusClient& operator=(ModbusClient&&) = delete;  // move assignment

    private:
        bool sendRequest(ModbusBuffer& request, ModbusBuffer& response, const timespec& to);

        OSEF::TcpClient client;
        OSEF::NetBufferSession session;
        uint16_t transactionId;
    };
}  // namespace OSEF

#endif /* DCMODBUSCLIENT_H */
