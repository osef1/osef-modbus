#include "ModbusResponseReadCoils.h"
#include "Debug.h"

bool OSEF::ModbusResponseReadCoils::checkResponse(ModbusRequestReadCoils& request, std::vector<bool>& coils)
{
    bool ret = false;

    if (ModbusBuffer::checkResponse(request))
    {
        uint8_t nBytes = 0;
        if (extractUInt8(nBytes))
        {
                if (nBytes == static_cast<uint8_t>(getSizeInBytes(coils.size())))
                {
                    uint8_t byte = 0;
                    size_t b = 0;
                    size_t c = 0;
                    do
                    {
                        ret = extractUInt8(byte);
                        if (ret)
                        {
                            b++;
                            for (uint8_t i = 0; (i < 8) && (c < coils.size()); i++, c++)
                            {
                                if ((byte & (1 << i)) != 0)
                                {
                                    coils[c] = true;
                                }
                                else
                                {
                                    coils[c] = false;
                                }
                            }
                        }
                    }while (ret && (b < nBytes) && (c < coils.size()));
                }
                else
                {
                    DERR("wrong byte count " << static_cast<uint32_t>(nBytes) << " != " << getSizeInBytes(coils.size()));
                }
        }
        else
        {
            DERR("error extracting bytes count");
        }
    }

    return ret;
}
