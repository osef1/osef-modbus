#ifndef DCMODBUSBOOLCONTAINER_H
#define DCMODBUSBOOLCONTAINER_H

#include <cstddef>  // size_t
#include <cstdint>  // uint16_t

#include <vector>

namespace OSEF
{
    class ModbusBoolContainer
    {
    public:
        explicit ModbusBoolContainer(const size_t& nbools);
        virtual ~ModbusBoolContainer();

        size_t getSize() const {return bools.size();}

        bool check(const size_t& adr) const;

        bool get(const size_t& adr, bool& val) const;
        bool getByte(const size_t& adr, const size_t& n, uint8_t& byte) const;

        bool set(const size_t& adr, const bool& val);
        bool set(const size_t& adr, const uint8_t& val, const uint8_t& size);
        bool set(const size_t& adr);
        bool reset(const size_t& adr);

        ModbusBoolContainer(const ModbusBoolContainer&) = delete;  // copy constructor
        ModbusBoolContainer& operator=(const ModbusBoolContainer&) = delete;  // copy assignment
        ModbusBoolContainer(ModbusBoolContainer&&) = delete;  // move constructor
        ModbusBoolContainer& operator=(ModbusBoolContainer&&) = delete;  // move assignment

    private:
        std::vector<bool> bools;
    };
}  // namespace OSEF

#endif /* DCMODBUSBOOLCONTAINER_H */
