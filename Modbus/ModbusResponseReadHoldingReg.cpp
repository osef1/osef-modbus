#include "ModbusResponseReadHoldingReg.h"
#include "Debug.h"

bool OSEF::ModbusResponseReadHoldingReg::checkResponse(ModbusRequestReadHoldingReg& request, uint16_t& reg)
{
    bool ret = false;

    if (ModbusBuffer::checkResponse(request))
    {
        uint8_t byteCount = 0;
        if (extractUInt8(byteCount))
        {
            if (byteCount == 2)
            {
                ret = extractUInt16(reg);
            }
            else
            {
                DERR("wrong byte count " << byteCount << " != 2");
            }
        }
        else
        {
            DERR("error extracting bytes count");
        }
    }

    return ret;
}
