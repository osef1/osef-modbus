#ifndef DCMODBUSSERVER_H
#define DCMODBUSSERVER_H

#include "ModbusBuffer.h"
#include "ModbusBoolContainer.h"
#include "ModbusRegContainer.h"
#include "Mutex.h"

#include <string>

namespace OSEF
{
    class ModbusServer
    {
    public:
        ModbusServer(const uint16_t& ncoils, const uint16_t& ninputs, const uint16_t& nhregs, const uint16_t& niregs = 0);
        virtual ~ModbusServer() = default;

        bool serveRequest(ModbusBuffer& req, ModbusBuffer& resp);

        bool setInput(const uint16_t& adr, const bool& val);
        bool setInputReg(const uint16_t& adr, const uint16_t& val);

        size_t getCoilsQuantity() const {return coils.getSize();}

        bool setVendorName(const std::string& name) {return setDeviceIdentification(0, name);}
        bool setProductCode(const std::string& code) {return setDeviceIdentification(1, code);}
        bool setRevision(const std::string& version) {return setDeviceIdentification(2, version);}
        bool setVendorURL(const std::string& URL) {return setDeviceIdentification(3, URL);}
        bool setProductName(const std::string& name) {return setDeviceIdentification(4, name);}
        bool setModelName(const std::string& name) {return setDeviceIdentification(5, name);}
        bool setUserApplicationName(const std::string& name) {return setDeviceIdentification(6, name);}

        bool getVendorName(std::string& name) {return getDeviceIdentification(0, name);}
        bool getProductCode(std::string& code) {return getDeviceIdentification(1, code);}
        bool getRevision(std::string& version) {return getDeviceIdentification(2, version);}
        bool getVendorURL(std::string& URL) {return getDeviceIdentification(3, URL);}
        bool getProductName(std::string& name) {return getDeviceIdentification(4, name);}
        bool getModelName(std::string& name) {return getDeviceIdentification(5, name);}
        bool getUserApplicationName(std::string& name) {return getDeviceIdentification(6, name);}

        ModbusServer(const ModbusServer&) = delete;  // copy constructor
        ModbusServer& operator=(const ModbusServer&) = delete;  // copy assignment
        ModbusServer(ModbusServer&&) = delete;  // move constructor
        ModbusServer& operator=(ModbusServer&&) = delete;  // move assignment

    private:
        bool setDeviceIdentification(const size_t& devid, const std::string& object);
        bool getDeviceIdentification(const size_t& devid, std::string& object);

        bool serveFunction(const uint8_t& fc, ModbusBuffer& req, ModbusBuffer& resp);

        bool serveReadCoils(ModbusBuffer& req, ModbusBuffer& resp) const;
        bool respondReadCoils(ModbusBuffer& req, ModbusBuffer& resp, const uint16_t& adr, const uint16_t& n) const;

        bool serveReadInputs(ModbusBuffer& req, ModbusBuffer& resp) const;
        bool respondReadInputs(ModbusBuffer& req, ModbusBuffer& resp, const uint16_t& adr, const uint16_t& n) const;

        bool serveReadHoldingRegs(ModbusBuffer& req, ModbusBuffer& resp) const;
        bool respondReadHoldingRegs(ModbusBuffer& req, ModbusBuffer& resp, const uint16_t& adr, const uint16_t& n) const;

        bool serveReadInputRegs(ModbusBuffer& req, ModbusBuffer& resp) const;
        bool respondReadInputRegs(ModbusBuffer& req, ModbusBuffer& resp, const uint16_t& adr, const uint16_t& n) const;

        bool serveWriteSingleCoil(ModbusBuffer& req, ModbusBuffer& resp);
        bool respondWriteSingleCoil(ModbusBuffer& req, ModbusBuffer& resp, const uint16_t& adr, const uint16_t& val);

        bool serveWriteSingleHoldingReg(ModbusBuffer& req, ModbusBuffer& resp);
        bool respondWriteSingleHoldingReg(ModbusBuffer& req, ModbusBuffer& resp, const uint16_t& adr, const uint16_t& val);

        bool serveWriteMultipleCoils(ModbusBuffer& req, ModbusBuffer& resp);
        bool respondWriteMultipleCoils(ModbusBuffer& req, ModbusBuffer& resp, const uint16_t& adr, const uint16_t& nCoils, const uint8_t& nBytes);
        bool writeMultipleCoils(ModbusBuffer& req, const uint16_t& adr, const uint16_t& nCoils, const uint8_t& nBytes);

        bool serveWriteMaskReg(ModbusBuffer& req, ModbusBuffer& resp);
        bool respondWriteMaskReg(ModbusBuffer& req, ModbusBuffer& resp, const uint16_t& adr, const uint16_t& andmask, const uint16_t& ormask);

        bool serveEncapsulatedInterface(ModbusBuffer& req, ModbusBuffer& resp);
        bool respondReadDeviceIdentification(ModbusBuffer& req, ModbusBuffer& resp);
        bool respondReadDeviceIdentificationStream(ModbusBuffer& req, ModbusBuffer& resp, const uint8_t& rc, const uint8_t& foi, const uint8_t& loi);
        bool pushDeviceIdentificationStream(ModbusBuffer& resp, const uint8_t& foi, const uint8_t& loi);
        bool respondReadDeviceIdentificationIndividual(ModbusBuffer& req, ModbusBuffer& resp, const uint8_t& oi);

        ModbusBoolContainer coils;
        ModbusBoolContainer inputs;
        ModbusRegContainer holdingRegs;
        ModbusRegContainer inputRegs;
        OSEF::Mutex mutex;

        std::string deviceIdentification[7];
    };
}  // namespace OSEF

#endif /* DCMODBUSSERVER_H */
