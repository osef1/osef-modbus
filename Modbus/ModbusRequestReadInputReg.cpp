#include "ModbusRequestReadInputReg.h"
#include "Debug.h"

OSEF::ModbusRequestReadInputReg::ModbusRequestReadInputReg(const uint16_t& adr, const uint16_t& qty)
    :ModbusBuffer(4)
{
    if (setFunctionCode(4))
    {
        if (setAddress(adr))
        {
            if (not setQuantity(qty))
            {
                DERR("error initializing input register quantity");
            }
        }
        else
        {
            DERR("error initializing input register address");
        }
    }
    else
    {
        DERR("error initializing read input register function code");
    }
}

OSEF::ModbusRequestReadInputReg::~ModbusRequestReadInputReg() = default;
