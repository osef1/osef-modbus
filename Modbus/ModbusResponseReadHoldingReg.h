#ifndef DCMODBUSRESPONSEREADHOLDINGREG_H
#define DCMODBUSRESPONSEREADHOLDINGREG_H

#include "ModbusRequestReadHoldingReg.h"

namespace OSEF
{
    class ModbusResponseReadHoldingReg : public ModbusBuffer
    {
    public:
        ModbusResponseReadHoldingReg() = default;
        ~ModbusResponseReadHoldingReg() override = default;

        bool checkResponse(ModbusRequestReadHoldingReg& request, uint16_t& reg);  // check buffer and extract read register

        ModbusResponseReadHoldingReg(const ModbusResponseReadHoldingReg&) = delete;  // copy constructor
        ModbusResponseReadHoldingReg& operator=(const ModbusResponseReadHoldingReg&) = delete;  // copy assignment
        ModbusResponseReadHoldingReg(ModbusResponseReadHoldingReg&&) = delete;  // move constructor
        ModbusResponseReadHoldingReg& operator=(ModbusResponseReadHoldingReg&&) = delete;  // move assignment
    };
}  // namespace OSEF

#endif /* DCMODBUSRESPONSEREADHOLDINGREG_H */
