#ifndef DCMODBUSREQUESTREADINPUTREG_H
#define DCMODBUSREQUESTREADINPUTREG_H

#include "ModbusBuffer.h"

namespace OSEF
{
    class ModbusRequestReadInputReg : public ModbusBuffer
    {
    public:
        explicit ModbusRequestReadInputReg(const uint16_t& adr, const uint16_t& qty = 1);
        ~ModbusRequestReadInputReg() override;

        ModbusRequestReadInputReg(const ModbusRequestReadInputReg&) = delete;  // copy constructor
        ModbusRequestReadInputReg& operator=(const ModbusRequestReadInputReg&) = delete;  // copy assignment
        ModbusRequestReadInputReg(ModbusRequestReadInputReg&&) = delete;  // move constructor
        ModbusRequestReadInputReg& operator=(ModbusRequestReadInputReg&&) = delete;  // move assignment
    };
}  // namespace OSEF

#endif /* DCMODBUSREQUESTREADINPUTREG_H */
