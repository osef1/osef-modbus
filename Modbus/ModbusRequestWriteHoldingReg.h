#ifndef DCMODBUSREQUESTWRITEHOLDINGREG_H
#define DCMODBUSREQUESTWRITEHOLDINGREG_H

#include "ModbusBuffer.h"

namespace OSEF
{
    class ModbusRequestWriteHoldingReg : public ModbusBuffer
    {
    public:
        explicit ModbusRequestWriteHoldingReg(const uint16_t& adr, const uint16_t& val = 0);
        ~ModbusRequestWriteHoldingReg() override;

        ModbusRequestWriteHoldingReg(const ModbusRequestWriteHoldingReg&) = delete;  // copy constructor
        ModbusRequestWriteHoldingReg& operator=(const ModbusRequestWriteHoldingReg&) = delete;  // copy assignment
        ModbusRequestWriteHoldingReg(ModbusRequestWriteHoldingReg&&) = delete;  // move constructor
        ModbusRequestWriteHoldingReg& operator=(ModbusRequestWriteHoldingReg&&) = delete;  // move assignment
    };
}  // namespace OSEF

#endif /* DCMODBUSREQUESTWRITEHOLDINGREG_H */
