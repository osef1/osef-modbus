#ifndef DCMODBUSRESPONSEREADCOILS_H
#define DCMODBUSRESPONSEREADCOILS_H

#include "ModbusRequestReadCoils.h"

#include <vector>

namespace OSEF
{
    class ModbusResponseReadCoils : public ModbusBuffer
    {
    public:
        ModbusResponseReadCoils() = default;
        ~ModbusResponseReadCoils() override = default;

        bool checkResponse(ModbusRequestReadCoils& request, std::vector<bool>& coils);  // check buffer and extract read registers

        ModbusResponseReadCoils(const ModbusResponseReadCoils&) = delete;  // copy constructor
        ModbusResponseReadCoils& operator=(const ModbusResponseReadCoils&) = delete;  // copy assignment
        ModbusResponseReadCoils(ModbusResponseReadCoils&&) = delete;  // move constructor
        ModbusResponseReadCoils& operator=(ModbusResponseReadCoils&&) = delete;  // move assignment
    };
}  // namespace OSEF

#endif /* DCMODBUSRESPONSEREADCOILS_H */
