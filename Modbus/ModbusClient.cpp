#include "ModbusClient.h"
#include "Debug.h"
#include "ModbusResponseReadCoils.h"
#include "ModbusResponseWriteCoils.h"
#include "ModbusResponseReadInputReg.h"
#include "ModbusResponseReadHoldingReg.h"
#include "ModbusResponseWriteHoldingReg.h"

OSEF::ModbusClient::ModbusClient(const std::string& h, const std::string& p)
    : client(h, p),
    session(&client),
    transactionId(0) {}

OSEF::ModbusClient::~ModbusClient() = default;

bool OSEF::ModbusClient::sendRequest(ModbusBuffer& request, ModbusBuffer& response, const timespec& to)
{
    bool ret = false;

    if (request.setTransactionId(transactionId))
    {
        transactionId++;

        if (session.sendBuffer(request))
        {
            ret = session.receiveBuffer(response, to);
        }
    }
    else
    {
        DERR("message setting transaction id");
    }

    return ret;
}

bool OSEF::ModbusClient::readCoils(const uint16_t& adr, std::vector<bool>& coils, const timespec& to)
{
    bool ret = false;

    ModbusRequestReadCoils request(adr, coils.size());
    ModbusResponseReadCoils response;

    if (sendRequest(request, response, to))
    {
        ret = response.checkResponse(request, coils);
    }

    return ret;
}

bool OSEF::ModbusClient::writeCoils(const uint16_t& adr, const std::vector<bool>& coils, const timespec& to)
{
    bool ret = false;

    ModbusRequestWriteCoils request(adr, coils);
    ModbusResponseWriteCoils response;

    if (sendRequest(request, response, to))
    {
        ret = response.checkResponse(request);
    }

    return ret;
}

bool OSEF::ModbusClient::readHoldingReg(const uint16_t& adr, uint16_t& reg, const timespec& to)
{
    bool ret = false;

    ModbusRequestReadHoldingReg request(adr);
    ModbusResponseReadHoldingReg response;

    if (sendRequest(request, response, to))
    {
        ret = response.checkResponse(request, reg);
    }

    return ret;
}

bool OSEF::ModbusClient::readInputReg(const uint16_t& adr, const uint16_t& qty, std::vector<uint16_t>& regs, const timespec& to)
{
    bool ret = false;

    ModbusRequestReadInputReg request(adr, qty);
    ModbusResponseReadInputReg response;

    if (sendRequest(request, response, to))
    {
        ret = response.checkResponse(request, regs);
    }

    return ret;
}

bool OSEF::ModbusClient::writeHoldingReg(const uint16_t& adr, const uint16_t& val, const timespec& to)
{
    bool ret = false;

    ModbusRequestWriteHoldingReg request(adr, val);
    ModbusResponseWriteHoldingReg response;

    if (sendRequest(request, response, to))
    {
        ret = response.checkResponse(request);
    }

    return ret;
}
