#ifndef OSEFMODBUSAPPLICATION_H
#define OSEFMODBUSAPPLICATION_H

#include "TcpApplication.h"
#include "ModbusServer.h"

namespace OSEF
{
    class ModbusApplication : public TcpApplication
    {
    public:
        ModbusApplication(const uint16_t& ncoils, const uint16_t& ninputs, const uint16_t& nhregs, const uint16_t& niregs = 0);
        ~ModbusApplication() override = default;

        bool serve(TcpServer& server) override;

        ModbusApplication(const ModbusApplication&) = delete;  // copy constructor
        ModbusApplication& operator=(const ModbusApplication&) = delete;  // copy assignment
        ModbusApplication(ModbusApplication&&) = delete;  // move constructor
        ModbusApplication& operator=(ModbusApplication&&) = delete;  // move assignment

    private:
        OSEF::ModbusServer modbusServer;
    };
}  // namespace OSEF

#endif /* OSEFMODBUSAPPLICATION_H */
