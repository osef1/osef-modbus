#ifndef DCMODBUSRESPONSEWRITEHOLDINGREG_H
#define DCMODBUSRESPONSEWRITEHOLDINGREG_H

#include "ModbusBuffer.h"
#include "ModbusRequestWriteHoldingReg.h"

namespace OSEF
{
    class ModbusResponseWriteHoldingReg : public ModbusBuffer
    {
    public:
        ModbusResponseWriteHoldingReg();
        ~ModbusResponseWriteHoldingReg() override = default;

        bool checkResponse(ModbusRequestWriteHoldingReg& request);  // check buffer and written register

        ModbusResponseWriteHoldingReg(const ModbusResponseWriteHoldingReg&) = delete;  // copy constructor
        ModbusResponseWriteHoldingReg& operator=(const ModbusResponseWriteHoldingReg&) = delete;  // copy assignment
        ModbusResponseWriteHoldingReg(ModbusResponseWriteHoldingReg&&) = delete;  // move constructor
        ModbusResponseWriteHoldingReg& operator=(ModbusResponseWriteHoldingReg&&) = delete;  // move assignment

    private:
        bool checkAddress(ModbusRequestWriteHoldingReg& request);  // check written register address
        bool checkValue(ModbusRequestWriteHoldingReg& request);  // check written register value
    };
}  // namespace OSEF

#endif /* DCMODBUSRESPONSEWRITEHOLDINGREG_H */
