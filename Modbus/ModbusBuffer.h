#ifndef DCMODBUSBUFFER_H
#define DCMODBUSBUFFER_H

#include "NetBuffer.h"
#include "ModbusRegContainer.h"
#include "ModbusBoolContainer.h"

#include <string>

// MBAP = TID + PID + LG + UID

// TID  2B  Transation Id, set by client to sort responses
// PID  2B  Protocol Id, 0 by default
// LG   2B  Length, including Unit Id, in bytes
// UID  1B  Unit ID, slave device

// Function                 FC      Request             Response
// Read coils               0x01    FC, SA, Q           FC, BC, D
// Read discrete inputs     0x02    FC, SA, Q           FC, BC, D
// Read holding registers   0x03    FC, SA, Q           FC, BC, D
// Read input registers     0x04    FC, SA, Q           FC, BC, D
// Write single coil        0x05    FC, A, V            FC, A, V
// Write single register    0x06    FC, A, V            FC, A, V
// Write multiple coils     0x0F    FC, SA, Q, BC, D    FC, SA, Q
// Write multiple registers 0x10    FC, SA, Q, BC, D    FC, SA, Q

// Modbus encapsulated interfaces   FC      Request         Response
// Read device identification       0x2b    FC, MEI, RC, OI FC, MEI, RC, CL, MF, NI, NO, OI(n), OL(n), OV(n)

// FC   1B  Function Code
// SA/A 2B  Starting Address / Address
// Q    2B  Quantity of coils [1-2000R/1968W], inputs[1-2000], registers [1-125R/123W]
// V    2B  2 bytes Data
// BC   1B  Byte count [1-250]
// D    xB  BC bytes Data
// MEI  1B  Modbus Encapsulated Interface
// RC   1B  Read device id Code
// OI   1B  Object Id
// CL   1B  Conformity Level
// MF   1B  More Follows
// NI   1B  Next object Id
// NO   1B  Number of Objects
// OL   1B  Object Length
// OV   xB  Object Value

namespace OSEF
{
    class ModbusBuffer : public OSEF::NetBuffer
    {
    public:
        explicit ModbusBuffer(const size_t& addOffset = 0);
        ~ModbusBuffer() override = default;

        bool resetToResponse(ModbusBuffer& req, const uint8_t& fc);  // reset buffer and initialize transaction id
        bool resetToException(ModbusBuffer& req, const uint8_t& fc, const uint8_t& exc);

        bool setTransactionId(const uint16_t& trid);
        bool setUnitId(const uint8_t& unid);
        bool setFunctionCode(const uint8_t& fc);  // standard function code in [1-127], [65-72] and [100-110] are user defined
        bool setAddress(const uint16_t& adr);
        bool setValue(const uint16_t& val);
        bool setQuantity(const uint16_t& q);
        bool setAndMask(const uint16_t& m);
        bool setOrMask(const uint16_t& m);

        bool getTransactionId(uint16_t& trid);
        bool getProtocolId(uint16_t& prid);
        bool getLength(uint16_t& length);
        bool getUnitId(uint8_t& unid);
        bool getFunctionCode(uint8_t& fc);
        bool getAddress(uint16_t& adr);
        bool getValue(uint16_t& val);
        bool getQuantity(uint16_t& q);
        bool getAndMask(uint16_t& m);
        bool getOrMask(uint16_t& m);

        bool setEncapsulatedInterface(const uint8_t& mei);
        bool setReadDeviceIdCode(const uint8_t& rc);
        bool setObjectId(const uint8_t& oi);
        bool setConformityLevel(const uint8_t& cl);
        bool setMorefollows(const bool& mf);
        bool setNextObjectId(const uint8_t& ni);
        bool setNumberOfObjects(const uint8_t& no);

        bool getEncapsulatedInterface(uint8_t& mei);
        bool getReadDeviceIdCode(uint8_t& rc);
        bool getObjectId(uint8_t& oi);

        bool getConformityLevel(uint8_t& cl);
        bool getMorefollows(bool& mf);
        bool getNextObjectId(uint8_t& ni);
        bool getNumberOfObjects(uint8_t& no);

        bool pushDeviceIdentificationObject(const uint8_t& oi, const std::string& obj);

        uint16_t getSizeInBytes(const uint16_t& nBits) const;  // number of bytes to store nBits bits

        size_t pushRegs(const ModbusRegContainer& regs, const size_t& adr, const size_t& n);
        size_t pushBoolsBytes(const ModbusBoolContainer& bools, const size_t& adr, const size_t& n);

        bool checkResponse(ModbusBuffer& request);  // check buffer has same transaction id and function code as request

        ModbusBuffer(const ModbusBuffer&) = delete;  // copy constructor
        ModbusBuffer& operator=(const ModbusBuffer&) = delete;  // copy assignment
        ModbusBuffer(ModbusBuffer&&) = delete;  // move constructor
        ModbusBuffer& operator=(ModbusBuffer&&) = delete;  // move assignment

    protected:
        bool setLimits(const size_t& dl, const size_t& offset, const size_t& offsetmin) override;

    private:
        bool setProtocolId(const uint16_t& prid);
        bool setLength(const uint16_t& length);

         bool pushException(const uint8_t& fc, const uint8_t& exc);  // exception code in [128-255] = function code + 0x80
    };
}  // namespace OSEF

#endif /* OSEFMODBUSBUFFER_H */
