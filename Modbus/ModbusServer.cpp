#include "ModbusServer.h"
#include "Debug.h"

OSEF::ModbusServer::ModbusServer(const uint16_t& ncoils, const uint16_t& ninputs, const uint16_t& nhregs, const uint16_t& niregs)
    :coils(ncoils),
    inputs(ninputs),
    holdingRegs(nhregs),
    inputRegs(niregs),

    deviceIdentification
        {
            "vendor name",
            "product code",
            "revision",
            "vendor URL",
            "product name",
            "model name",
            "user application name"
        }{}

bool OSEF::ModbusServer::getDeviceIdentification(const size_t& devid, std::string& object)
{
    bool ret = false;

    if (mutex.lock())
    {
        if (devid <= 6UL)
        {
            object.append(deviceIdentification[devid]);
            ret = true;
        }

        if (not mutex.unlock())
        {
            DERR("error unlocking mutex");
        }
    }
    else
    {
        DERR("error locking mutex");
    }

    return ret;
}

bool OSEF::ModbusServer::setDeviceIdentification(const size_t& devid, const std::string& object)
{
    bool ret = false;

    if (mutex.lock())
    {
        if (devid <= 6UL)
        {
            if (object.size() <= 255UL)
            {
                deviceIdentification[devid] = object;
                ret = true;
            }
        }

        if (not mutex.unlock())
        {
            DERR("error unlocking mutex");
        }
    }
    else
    {
        DERR("error locking mutex");
    }

    return ret;
}

bool OSEF::ModbusServer::setInput(const uint16_t& adr, const bool& val)
{
    bool ret = false;

    if (mutex.lock())
    {
        ret = inputs.set(adr, val);

        if (not mutex.unlock())
        {
            DERR("error unlocking mutex");
        }
    }
    else
    {
        DERR("error locking mutex");
    }

    return ret;
}

bool OSEF::ModbusServer::setInputReg(const uint16_t& adr, const uint16_t& val)
{
    bool ret = false;

    if (mutex.lock())
    {
        ret = inputRegs.set(adr, val);

        if (not mutex.unlock())
        {
            DERR("error unlocking mutex");
        }
    }
    else
    {
        DERR("error locking mutex");
    }

    return ret;
}

bool OSEF::ModbusServer::serveRequest(ModbusBuffer& req, ModbusBuffer& resp)
{
    bool ret = false;

    if (mutex.lock())
    {
        uint8_t fc = 0;
        if (req.getFunctionCode(fc))
        {
            ret = serveFunction(fc, req, resp);
        }

        if (not mutex.unlock())
        {
            DERR("error unlocking mutex");
        }
    }
    else
    {
        DERR("error locking mutex");
    }

    return ret;
}

bool OSEF::ModbusServer::serveFunction(const uint8_t& fc, ModbusBuffer& req, ModbusBuffer& resp)
{
    bool ret = false;

    DOUT("serving function " << +fc);

    switch (fc)
    {
        case 1U:
            ret = serveReadCoils(req, resp);
            break;
        case 2U:
            ret = serveReadInputs(req, resp);
            break;
        case 3U:
            ret = serveReadHoldingRegs(req, resp);
            break;
        case 4U:
            ret = serveReadInputRegs(req, resp);
            break;
        case 5U:
            ret = serveWriteSingleCoil(req, resp);
            break;
        case 6U:
            ret = serveWriteSingleHoldingReg(req, resp);
            break;
        case 15U:
            ret = serveWriteMultipleCoils(req, resp);
            break;
        case 22U:
            ret = serveWriteMaskReg(req, resp);
            break;
        case 43U:
            ret = serveEncapsulatedInterface(req, resp);
            break;
        default:
            ret = resp.resetToException(req, fc, 1);  // Illegal function
            break;
    }

    return ret;
}

bool OSEF::ModbusServer::serveReadCoils(ModbusBuffer& req, ModbusBuffer& resp) const
{
    bool ret = false;

//    if (req.increaseGetZone(4))
//    {
        uint16_t adr = 0;
        if (req.getAddress(adr))
        {
            uint16_t nCoils = 0;
            if (req.getQuantity(nCoils))
            {
                ret = respondReadCoils(req, resp, adr, nCoils);
            }
        }
//    }

    return ret;
}

bool OSEF::ModbusServer::respondReadCoils(ModbusBuffer& req, ModbusBuffer& resp, const uint16_t& adr, const uint16_t& nCoils) const
{
    bool ret = false;

    DOUT("reading " << nCoils << " coils from address " << adr);

    if ( (nCoils>static_cast<uint16_t>(0)) && (nCoils <= static_cast<uint16_t>(2000)) )
    {
        if (coils.check(adr) && coils.check(adr+(nCoils-1)))
        {
            if (resp.resetToResponse(req, 1))
            {
                const auto nBytes = static_cast<uint8_t>(resp.getSizeInBytes(nCoils));
                if (resp.pushUInt8(nBytes))  // Byte count
                {
                    if (resp.pushBoolsBytes(coils, adr, nCoils) == nBytes)
                    {
                        ret = true;
                    }
                }
            }
        }
        else
        {
            ret = resp.resetToException(req, 1, 2);  // Illegal data address
        }
    }
    else
    {
        ret = resp.resetToException(req, 1, 3);  // Illegal data value
    }

    return ret;
}

 bool OSEF::ModbusServer::serveReadInputs(ModbusBuffer& req, ModbusBuffer& resp) const
 {
    bool ret = false;

//    if (req.increaseGetZone(4))
//    {
        uint16_t adr = 0;
        if (req.getAddress(adr))
        {
            uint16_t n = 0;
            if (req.getQuantity(n))
            {
                ret = respondReadInputs(req, resp, adr, n);
            }
        }
//    }

    return ret;
 }

 bool OSEF::ModbusServer::respondReadInputs(ModbusBuffer& req, ModbusBuffer& resp, const uint16_t& adr, const uint16_t& n) const
{
    bool ret = false;

    DOUT("reading " << n << " inputs from address " << adr);

    if ( (n>static_cast<uint16_t>(0)) && (n <= static_cast<uint16_t>(2000)) )
    {
        if (inputs.check(adr) && inputs.check(adr+(n-1)))
        {
            if (resp.resetToResponse(req, 2))  // function code
            {
                const uint16_t nBytes = resp.getSizeInBytes(n);
                if (resp.pushUInt8(nBytes))  // Byte count
                {
                    if (resp.pushBoolsBytes(inputs, adr, n) == nBytes)
                    {
                        ret = true;
                    }
                }
            }
        }
        else
        {
            ret = resp.resetToException(req, 2, 2);  // Illegal data address
        }
    }
    else
    {
        ret = resp.resetToException(req, 2, 3);  // Illegal data value
    }

    return ret;
}

bool  OSEF::ModbusServer::serveReadHoldingRegs(ModbusBuffer& req, ModbusBuffer& resp) const
{
    bool ret = false;

//    if (req.increaseGetZone(4))
//    {
        uint16_t adr = 0;
        if (req.getAddress(adr))
        {
            uint16_t n = 0;
            if (req.getQuantity(n))
            {
                ret = respondReadHoldingRegs(req, resp, adr, n);
            }
        }
//    }

    return ret;
}

bool OSEF::ModbusServer::respondReadHoldingRegs(ModbusBuffer& req, ModbusBuffer& resp, const uint16_t& adr, const uint16_t& n) const
{
    bool ret = false;

    if ( (n>static_cast<uint16_t>(0)) && (n <= static_cast<uint16_t>(125)) )
    {
        if (holdingRegs.check(adr) && holdingRegs.check(adr+(n-1)))
        {
            if (resp.resetToResponse(req, 3))  // function code
            {
                if (resp.pushUInt8(n*2))  // Byte count
                {
                    if (resp.pushRegs(holdingRegs, adr, n) == n)
                    {
                        ret = true;
                    }
                }
            }
        }
        else
        {
            ret = resp.resetToException(req, 3, 2);  // Illegal data address
        }
    }
    else
    {
        ret = resp.resetToException(req, 3, 3);  // Illegal data value
    }

    return ret;
}

bool  OSEF::ModbusServer::serveReadInputRegs(ModbusBuffer& req, ModbusBuffer& resp) const
{
    bool ret = false;

//    if (req.increaseGetZone(4))
//    {
        uint16_t adr = 0;
        if (req.getAddress(adr))
        {
            uint16_t n = 0;
            if (req.getQuantity(n))
            {
                ret = respondReadInputRegs(req, resp, adr, n);
            }
        }
//    }

    return ret;
}

bool OSEF::ModbusServer::respondReadInputRegs(ModbusBuffer& req, ModbusBuffer& resp, const uint16_t& adr, const uint16_t& n) const
{
    bool ret = false;

    DOUT("reading " << n << " input regs from address " << adr);

    if ( (n>static_cast<uint16_t>(0)) && (n <= static_cast<uint16_t>(125)) )
    {
        if (inputRegs.check(adr) && inputRegs.check(adr+(n-1)))
        {
            if (resp.resetToResponse(req, 4))  // function code
            {
                if (resp.pushUInt8(n*2))  // Byte count
                {
                    if (resp.pushRegs(inputRegs, adr, n) == n)
                    {
                        ret = true;
                    }
                }
            }
        }
        else
        {
            ret = resp.resetToException(req, 4, 2);  // Illegal data address
        }
    }
    else
    {
        ret = resp.resetToException(req, 4, 3);  // Illegal data value
    }

    return ret;
}


bool OSEF::ModbusServer::serveWriteSingleCoil(ModbusBuffer& req, ModbusBuffer& resp)
{
    bool ret = false;

//    if (req.increaseGetZone(4))
//    {
        uint16_t adr = 0;
        if (req.getAddress(adr))
        {
            uint16_t val = 0;
            if (req.getValue(val))
            {
                ret = respondWriteSingleCoil(req, resp, adr, val);
            }
        }
//    }

    return ret;
}

bool OSEF::ModbusServer::respondWriteSingleCoil(ModbusBuffer& req, ModbusBuffer& resp, const uint16_t& adr, const uint16_t& val)
{
    bool ret = false;

    if ( (val == 0) || (val == 0xff00) )
    {
        if (coils.check(adr))
        {
            if (coils.set(adr, val != 0U))
            {
                if (resp.resetToResponse(req, 5))  //, 4))  // function code
                {
                    if (resp.setAddress(adr))  // coil address
                    {
                        ret = resp.setValue(val);  // coil value
                    }
                }
            }
            else
            {
                ret = resp.resetToException(req, 5, 4);  // Slave device failure
            }
        }
        else
        {
            ret = resp.resetToException(req, 5, 2);  // Illegal data address
        }
    }
    else
    {
        ret = resp.resetToException(req, 5, 3);  // Illegal data value
    }

    return ret;
}

bool OSEF::ModbusServer::serveWriteSingleHoldingReg(ModbusBuffer& req, ModbusBuffer& resp)
{
    bool ret = false;

//    if (req.increaseGetZone(4))
//    {
        uint16_t adr = 0;
        if (req.getAddress(adr))
        {
            uint16_t val = 0;
            if (req.getValue(val))
            {
                ret = respondWriteSingleHoldingReg(req, resp, adr, val);
            }
        }
//    }

    return ret;
}

bool OSEF::ModbusServer::respondWriteSingleHoldingReg(ModbusBuffer& req, ModbusBuffer& resp, const uint16_t& adr, const uint16_t& val)
{
    bool ret = false;

    if (holdingRegs.check(adr))
    {
        if (holdingRegs.set(adr, val))
        {
            if (resp.resetToResponse(req, 6))  //, 4))  // function code
            {
                if (resp.setAddress(adr))  // reg address
                {
                    ret = resp.setValue(val);  // written value
                }
            }
        }
        else
        {
            ret = resp.resetToException(req, 6, 4);  // Slave device failure
        }
    }
    else
    {
        ret = resp.resetToException(req, 6, 2);  // Illegal data address
    }

    return ret;
}

bool OSEF::ModbusServer::serveWriteMaskReg(ModbusBuffer& req, ModbusBuffer& resp)
{
    bool ret = false;

//    if (req.increaseGetZone(6))
//    {
        uint16_t adr = 0;
        if (req.getAddress(adr))
        {
            uint16_t am = 0;
            if (req.getAndMask(am))
            {
                uint16_t om = 0;
                if (req.getOrMask(om))
                {
                    ret = respondWriteMaskReg(req, resp, adr, am, om);
                }
            }
        }
//    }

    return ret;
}

bool OSEF::ModbusServer::respondWriteMaskReg(ModbusBuffer& req, ModbusBuffer& resp, const uint16_t& adr, const uint16_t& andmask, const uint16_t& ormask)
{
    bool ret = false;

    if (holdingRegs.check(adr))
    {
        uint16_t reg = 0;
        if (holdingRegs.get(adr, reg))
        {
            reg = (reg & andmask) | (ormask & ~andmask);
            if (holdingRegs.set(adr, reg))
            {
                if (resp.resetToResponse(req, 22))  //, 6))  // function code
                {
                    if (resp.setAddress(adr))
                    {
                        if (resp.setAndMask(andmask))
                        {
                            ret = resp.setOrMask(ormask);
                        }
                    }
                }
            }
            else
            {
                ret = resp.resetToException(req, 22, 4);  // Slave device failure
            }
        }
        else
        {
            ret = resp.resetToException(req, 22, 4);  // Slave device failure
        }
    }
    else
    {
        ret = resp.resetToException(req, 22, 2);  // Illegal data address
    }

    return ret;
}


bool OSEF::ModbusServer::serveWriteMultipleCoils(ModbusBuffer& req, ModbusBuffer& resp)
{
    bool ret = false;

//    if (req.increaseGetZone(4))
//    {
        uint16_t adr = 0;
        if (req.getAddress(adr))
        {
            uint16_t nCoils = 0;
            if (req.getQuantity(nCoils))
            {
                uint8_t nBytes = 0;
                if (req.extractUInt8(nBytes))
                {
                    ret = respondWriteMultipleCoils(req, resp, adr, nCoils, nBytes);
                }
            }
        }
//    }

    return ret;
}

bool OSEF::ModbusServer::writeMultipleCoils(ModbusBuffer& req, const uint16_t& adr, const uint16_t& nCoils, const uint8_t& nBytes)
{
    bool ret = false;

    uint8_t byte = 0;
    do
    {
        uint8_t coilsByte = 0;
        ret = req.extractUInt8(coilsByte);
        if (ret)
        {
            if (byte < static_cast<uint8_t>(nBytes-1U))
            {
                ret = coils.set(adr+(byte*8U), coilsByte, 8U);
            }
            else
            {
                if ((nCoils%8) > 0)
                {
                    ret = coils.set(adr+(byte*8U), coilsByte, nCoils-(nBytes*8U));
                }
                else
                {
                    ret = coils.set(adr+(byte*8U), coilsByte, 8U);
                }
            }
        }
        byte++;
    }while ( (byte < nBytes) && ret);

    return ret;
}

bool OSEF::ModbusServer::respondWriteMultipleCoils(ModbusBuffer& req, ModbusBuffer& resp, const uint16_t& adr, const uint16_t& nCoils, const uint8_t& nBytes)
{
    bool ret = false;

    DOUT("writing " << nCoils << " coils from address " << adr);

    const auto checkBytes = static_cast<uint8_t>(resp.getSizeInBytes(nCoils));
    if ( (nCoils>static_cast<uint16_t>(0)) && (nCoils <= static_cast<uint16_t>(1968)) && (nBytes == checkBytes) )
    {
        if (coils.check(adr) && coils.check(adr+(nCoils-1)) )
        {
//            uint8_t byte = 0;
//            do
//            {
//                uint8_t coilsByte = 0;
//                ret = req.extractUInt8(coilsByte);
//                if (ret)
//                {
//                    if (byte < static_cast<uint8_t>(nBytes-1U))
//                    {
//                        ret = coils.set(adr+(byte*8U), coilsByte, 8U);
//                    }
//                    else
//                    {
//                        if ((nCoils%8) > 0)
//                        {
//                            ret = coils.set(adr+(byte*8U), coilsByte, nCoils-(nBytes*8U));
//                        }
//                        else
//                        {
//                            ret = coils.set(adr+(byte*8U), coilsByte, 8U);
//                        }
//                    }
//                }
//                byte++;
//            }while ( (byte < nBytes) && ret);

            ret = writeMultipleCoils(req, adr, nCoils, nBytes);

            if (ret)
            {
                if (resp.resetToResponse(req, 15))  //, 4))
                {
                    if (resp.setAddress(adr))
                    {
                        ret = resp.setQuantity(nCoils);
                    }
                }
            }
            else
            {
                ret = resp.resetToException(req, 15, 4);  // Slave device failure
            }
        }
        else
        {
            ret = resp.resetToException(req, 15, 2);  // Illegal data address
        }
    }
    else
    {
        ret = resp.resetToException(req, 15, 3);  // Illegal data value
    }

    return ret;
}

bool OSEF::ModbusServer::serveEncapsulatedInterface(ModbusBuffer& req, ModbusBuffer& resp)
{
    bool ret = false;

//    if (req.increaseGetZone(3))  // interface, device & object
//    {
        uint8_t mei = 0;
        if (req.getEncapsulatedInterface(mei))
        {
            if (mei == 14)
            {
                ret = respondReadDeviceIdentification(req, resp);
            }
            else
            {
                ret = resp.resetToException(req, 43, 1);  // Illegal function code (interface)
            }
        }
//    }

    return ret;
}

bool OSEF::ModbusServer::respondReadDeviceIdentification(ModbusBuffer& req, ModbusBuffer& resp)
{
    bool ret = false;

    uint8_t rc = 0;
    if (req.getReadDeviceIdCode(rc))
    {
        uint8_t oi = 0;
        if (req.getObjectId(oi))
        {
            switch (rc)
            {
                case 1U:
                    ret = respondReadDeviceIdentificationStream(req, resp, rc, oi, 2);
                    break;
                case 2U:
                    ret = respondReadDeviceIdentificationStream(req, resp, rc, oi, 6);
                    break;
                case 3U:
                    break;
                case 4U:
                    ret = respondReadDeviceIdentificationIndividual(req, resp, oi);
                    break;
                default:
                    ret = resp.resetToException(req, 43, 3);  // Illegal data value
                    break;
            }
        }
    }

    return ret;
}

bool OSEF::ModbusServer::respondReadDeviceIdentificationStream(ModbusBuffer& req, ModbusBuffer& resp, const uint8_t& rc, const uint8_t& foi, const uint8_t& loi)
{
    bool ret = false;

    if (loi <= static_cast<uint8_t>(6))
    {
        if (resp.resetToResponse(req, 43))  //, 6))  // encapsulated interface function code
        {
            if (resp.setEncapsulatedInterface(14))  // read identification interface
            {
                if (resp.setReadDeviceIdCode(rc))  // individual acces code
                {
                    if (resp.setConformityLevel(0x82))  // 0x82 regular stream and individual access
                    {
                        if (resp.setMorefollows(false))  // eventually overwritten, space must be reserved
                        {
                            if (resp.setNextObjectId(0))  // eventually overwritten, space must be reserved
                            {
                                ret = pushDeviceIdentificationStream(resp, foi, loi);
                            }
                        }
                    }
                }
            }
        }
    }
    else
    {
        ret = resp.resetToException(req, 43, 2);  // Illegal data address
    }

    return ret;
}

bool OSEF::ModbusServer::pushDeviceIdentificationStream(ModbusBuffer& resp, const uint8_t& foi, const uint8_t& loi)
{
    bool ret = false;

    uint8_t i = foi;
    if (i > loi)
    {
       i = 0;
    }

    if (resp.setNumberOfObjects((loi-i)+static_cast<uint8_t>(1)))
    {
        uint8_t no = 0;
        do
        {
            if (resp.pushDeviceIdentificationObject(i, deviceIdentification[static_cast<size_t>(i)]))
            {
                no++;
                i++;
            }
            else
            {
                break;
            }
        }while (i <= loi);

        if (i > loi)
        {
            ret = true;
        }
        else
        {
            if (resp.setMorefollows(true))
            {
                if (resp.setNextObjectId(loi))
                {
                    ret = resp.setNumberOfObjects(no);
                }
            }
        }
    }

    return ret;
}

bool OSEF::ModbusServer::respondReadDeviceIdentificationIndividual(ModbusBuffer& req, ModbusBuffer& resp, const uint8_t& oi)
{
    bool ret = false;

    if (oi <= static_cast<uint8_t>(6))
    {
        if (resp.resetToResponse(req, 43))  // encapsulated interface function code
        {
            if (resp.setEncapsulatedInterface(14))  // read identification interface
            {
                if (resp.setReadDeviceIdCode(4))  // individual access code
                {
                    if (resp.setConformityLevel(0x82))
                    {
                        if (resp.setMorefollows(false))
                        {
                            if (resp.setNextObjectId(0))
                            {
                                if (resp.setNumberOfObjects(1))
                                {
                                    ret = resp.pushDeviceIdentificationObject(oi, deviceIdentification[static_cast<size_t>(oi)]);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    else
    {
        ret = resp.resetToException(req, 43, 2);  // Illegal data address
    }

    return ret;
}
