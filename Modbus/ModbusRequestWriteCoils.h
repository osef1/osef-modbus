#ifndef DCMODBUSREQUESTWRITECOILS_H
#define DCMODBUSREQUESTWRITECOILS_H

#include "ModbusBuffer.h"

#include <vector>

namespace OSEF
{
    class ModbusRequestWriteCoils : public ModbusBuffer
    {
    public:
        ModbusRequestWriteCoils(const uint16_t& adr, const std::vector<bool>& coils);
        ~ModbusRequestWriteCoils() override;

        ModbusRequestWriteCoils(const ModbusRequestWriteCoils&) = delete;  // copy constructor
        ModbusRequestWriteCoils& operator=(const ModbusRequestWriteCoils&) = delete;  // copy assignment
        ModbusRequestWriteCoils(ModbusRequestWriteCoils&&) = delete;  // move constructor
        ModbusRequestWriteCoils& operator=(ModbusRequestWriteCoils&&) = delete;  // move assignment
    };
}  // namespace OSEF

#endif /* DCMODBUSREQUESTWRITECOILS_H */
