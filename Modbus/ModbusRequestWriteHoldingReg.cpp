#include "ModbusRequestWriteHoldingReg.h"
#include "Debug.h"

OSEF::ModbusRequestWriteHoldingReg::ModbusRequestWriteHoldingReg(const uint16_t& adr, const uint16_t& val)
    :ModbusBuffer(4)
{
    if (setFunctionCode(6))
    {
        if (setAddress(adr))
        {
            if (not setValue(val))
            {
                DERR("error initializing holding register value");
            }
        }
        else
        {
            DERR("error initializing holding register address");
        }
    }
    else
    {
        DERR("error initializing write holding register function code");
    }
}

OSEF::ModbusRequestWriteHoldingReg::~ModbusRequestWriteHoldingReg() = default;
