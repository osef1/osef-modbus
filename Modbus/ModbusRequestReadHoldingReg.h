#ifndef DCMODBUSREQUESTREADHOLDINGREG_H
#define DCMODBUSREQUESTREADHOLDINGREG_H

#include "ModbusBuffer.h"

namespace OSEF
{
    class ModbusRequestReadHoldingReg : public ModbusBuffer
    {
    public:
        explicit ModbusRequestReadHoldingReg(const uint16_t& adr);
        ~ModbusRequestReadHoldingReg() override;

        ModbusRequestReadHoldingReg(const ModbusRequestReadHoldingReg&) = delete;  // copy constructor
        ModbusRequestReadHoldingReg& operator=(const ModbusRequestReadHoldingReg&) = delete;  // copy assignment
        ModbusRequestReadHoldingReg(ModbusRequestReadHoldingReg&&) = delete;  // move constructor
        ModbusRequestReadHoldingReg& operator=(ModbusRequestReadHoldingReg&&) = delete;  // move assignment
    };
}  // namespace OSEF

#endif /* DCMODBUSREQUESTREADHOLDINGREG_H */
