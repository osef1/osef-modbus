#ifndef DCMODBUSREGCONTAINER_H
#define DCMODBUSREGCONTAINER_H

#include <cstddef>  // size_t
#include <cstdint>  // uint16_t

#include <vector>

namespace OSEF
{
    class ModbusRegContainer
    {
    public:
        explicit ModbusRegContainer(const size_t& nregs);
        virtual ~ModbusRegContainer();

        bool check(const size_t& adr) const;
        bool get(const size_t& adr, uint16_t& val) const;
        bool set(const size_t& adr, const uint16_t& val);

        ModbusRegContainer(const ModbusRegContainer&) = delete;  // copy constructor
        ModbusRegContainer& operator=(const ModbusRegContainer&) = delete;  // copy assignment
        ModbusRegContainer(ModbusRegContainer&&) = delete;  // move constructor
        ModbusRegContainer& operator=(ModbusRegContainer&&) = delete;  // move assignment

    private:
        std::vector<uint16_t> regs;
    };
}  // namespace OSEF

#endif /* SFMBREGCONTAINER_H */
