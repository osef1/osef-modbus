#include "ModbusResponseReadInputReg.h"
#include "Debug.h"

bool OSEF::ModbusResponseReadInputReg::checkResponse(ModbusRequestReadInputReg& request, uint16_t& reg)
{
    bool ret = false;

    if (ModbusBuffer::checkResponse(request))
    {
        uint8_t byteCount = 0;
        if (extractUInt8(byteCount))
        {
            if (byteCount == 2)
            {
                ret = extractUInt16(reg);
            }
        }
    }

    return ret;
}

bool OSEF::ModbusResponseReadInputReg::checkResponse(ModbusRequestReadInputReg& request, std::vector<uint16_t>& regs)
{
    bool ret = false;

    if (ModbusBuffer::checkResponse(request))
    {
        uint8_t byteCount = 0;
        if (extractUInt8(byteCount))
        {
            if (byteCount == (2*regs.size()))
            {
                size_t i = 0;
                do
                {
                    ret = extractUInt16(regs[i]);
                    if (ret)
                    {
                        i++;
                    }
                }while (ret && (i < regs.size()));
            }
            else
            {
                DERR("wrong byte count " << static_cast<uint32_t>(byteCount) << " != " << 2*regs.size());
            }
        }
        else
        {
            DERR("error extracting bytes count");
        }
    }

    return ret;
}
