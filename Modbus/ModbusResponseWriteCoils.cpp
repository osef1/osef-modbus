#include "ModbusResponseWriteCoils.h"

OSEF::ModbusResponseWriteCoils::ModbusResponseWriteCoils()
    :ModbusBuffer(4) {}

bool OSEF::ModbusResponseWriteCoils::checkResponse(ModbusRequestWriteCoils& request)
{
    bool ret = false;

    if (ModbusBuffer::checkResponse(request))
    {
        if (checkAddress(request))
        {
            ret = checkQuantity(request);
        }
    }

    return ret;
}

bool OSEF::ModbusResponseWriteCoils::checkAddress(ModbusRequestWriteCoils& request)
{
    bool ret = false;

    uint16_t reqadr = 0;
    uint16_t rspadr = 0;
    if (request.getAddress(reqadr) && getAddress(rspadr))
    {
        if (reqadr == rspadr)
        {
            ret = true;
        }
    }

    return ret;
}

bool OSEF::ModbusResponseWriteCoils::checkQuantity(ModbusRequestWriteCoils& request)
{
    bool ret = false;

    uint16_t reqCoils = 0;
    uint16_t rspCoils = 0;
    if (request.getValue(reqCoils) && getValue(rspCoils))
    {
        if (reqCoils == rspCoils)
        {
            ret = true;
        }
    }

    return ret;
}
