#ifndef DCMODBUSRESPONSEWRITECOILS_H
#define DCMODBUSRESPONSEWRITECOILS_H

#include "ModbusRequestWriteCoils.h"

namespace OSEF
{
    class ModbusResponseWriteCoils : public ModbusBuffer
    {
    public:
        ModbusResponseWriteCoils();
        ~ModbusResponseWriteCoils() override = default;

        bool checkResponse(ModbusRequestWriteCoils& request);  // check buffer and written register

        ModbusResponseWriteCoils(const ModbusResponseWriteCoils&) = delete;  // copy constructor
        ModbusResponseWriteCoils& operator=(const ModbusResponseWriteCoils&) = delete;  // copy assignment
        ModbusResponseWriteCoils(ModbusResponseWriteCoils&&) = delete;  // move constructor
        ModbusResponseWriteCoils& operator=(ModbusResponseWriteCoils&&) = delete;  // move assignment

    private:
        bool checkAddress(ModbusRequestWriteCoils& request);  // check written register address
        bool checkQuantity(ModbusRequestWriteCoils& request);  // check written register value
    };
}  // namespace OSEF

#endif /* DCMODBUSRESPONSEWRITECOILS_H */
