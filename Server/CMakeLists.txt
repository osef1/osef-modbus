cmake_minimum_required(VERSION 3.13)

include(../osef.cmake)

project(osef-modbus-server)

add_subdirectory(".." "${CMAKE_CURRENT_BINARY_DIR}/Modbus")
link_directories(${CMAKE_CURRENT_BINARY_DIR}/Modbus)

add_subdirectory("../osef-netbuffer/osef-posix/Signal" "${CMAKE_CURRENT_BINARY_DIR}/Signal")
link_directories(${CMAKE_CURRENT_BINARY_DIR}/Signal)

#add_subdirectory("../osef-netbuffer/osef-posix/Time" "${CMAKE_CURRENT_BINARY_DIR}/Time")
#link_directories(${CMAKE_CURRENT_BINARY_DIR}/OSEF_Time)

add_executable(${PROJECT_NAME}
    main.cpp
)

target_link_libraries(${PROJECT_NAME}
    osef-modbus
    osef-signal
    #OSEF_Time
    pthread
)
