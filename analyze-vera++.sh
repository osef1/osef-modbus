#!/bin/bash

vera++ --version

# run analysis only if source are provided
if [ -z "$1" ]
then
    echo Please provide sources i.e. "*/*.h */*.cpp"
else
    vera++ --exclusions analyze-vera++-exclusions.txt --error --show-rule --summary "$@"
fi
