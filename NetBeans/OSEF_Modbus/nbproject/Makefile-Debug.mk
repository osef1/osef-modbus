#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux
CND_DLIB_EXT=so
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/_ext/8b36c9fe/ModbusApplication.o \
	${OBJECTDIR}/_ext/8b36c9fe/ModbusBoolContainer.o \
	${OBJECTDIR}/_ext/8b36c9fe/ModbusBuffer.o \
	${OBJECTDIR}/_ext/8b36c9fe/ModbusClient.o \
	${OBJECTDIR}/_ext/8b36c9fe/ModbusRegContainer.o \
	${OBJECTDIR}/_ext/8b36c9fe/ModbusRequestReadCoils.o \
	${OBJECTDIR}/_ext/8b36c9fe/ModbusRequestReadHoldingReg.o \
	${OBJECTDIR}/_ext/8b36c9fe/ModbusRequestReadInputReg.o \
	${OBJECTDIR}/_ext/8b36c9fe/ModbusRequestWriteCoils.o \
	${OBJECTDIR}/_ext/8b36c9fe/ModbusRequestWriteHoldingReg.o \
	${OBJECTDIR}/_ext/8b36c9fe/ModbusResponseReadCoils.o \
	${OBJECTDIR}/_ext/8b36c9fe/ModbusResponseReadHoldingReg.o \
	${OBJECTDIR}/_ext/8b36c9fe/ModbusResponseReadInputReg.o \
	${OBJECTDIR}/_ext/8b36c9fe/ModbusResponseWriteCoils.o \
	${OBJECTDIR}/_ext/8b36c9fe/ModbusResponseWriteHoldingReg.o \
	${OBJECTDIR}/_ext/8b36c9fe/ModbusServer.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=-DDEBUG -Wall -Wextra -Werror
CXXFLAGS=-DDEBUG -Wall -Wextra -Werror

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libosef_modbus.a

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libosef_modbus.a: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${RM} ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libosef_modbus.a
	${AR} -rv ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libosef_modbus.a ${OBJECTFILES} 
	$(RANLIB) ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libosef_modbus.a

${OBJECTDIR}/_ext/8b36c9fe/ModbusApplication.o: ../../Modbus/ModbusApplication.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/8b36c9fe
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../osef-netbuffer/osef-posix/osef-log/Debug -I../../osef-netbuffer/NetBuffer -I../../osef-netbuffer/osef-posix/Socket -I../../osef-netbuffer/osef-posix/Mutex -I../../osef-netbuffer/osef-posix/Thread -I../../osef-netbuffer/osef-posix/MsgQ -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/8b36c9fe/ModbusApplication.o ../../Modbus/ModbusApplication.cpp

${OBJECTDIR}/_ext/8b36c9fe/ModbusBoolContainer.o: ../../Modbus/ModbusBoolContainer.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/8b36c9fe
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../osef-netbuffer/osef-posix/osef-log/Debug -I../../osef-netbuffer/NetBuffer -I../../osef-netbuffer/osef-posix/Socket -I../../osef-netbuffer/osef-posix/Mutex -I../../osef-netbuffer/osef-posix/Thread -I../../osef-netbuffer/osef-posix/MsgQ -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/8b36c9fe/ModbusBoolContainer.o ../../Modbus/ModbusBoolContainer.cpp

${OBJECTDIR}/_ext/8b36c9fe/ModbusBuffer.o: ../../Modbus/ModbusBuffer.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/8b36c9fe
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../osef-netbuffer/osef-posix/osef-log/Debug -I../../osef-netbuffer/NetBuffer -I../../osef-netbuffer/osef-posix/Socket -I../../osef-netbuffer/osef-posix/Mutex -I../../osef-netbuffer/osef-posix/Thread -I../../osef-netbuffer/osef-posix/MsgQ -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/8b36c9fe/ModbusBuffer.o ../../Modbus/ModbusBuffer.cpp

${OBJECTDIR}/_ext/8b36c9fe/ModbusClient.o: ../../Modbus/ModbusClient.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/8b36c9fe
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../osef-netbuffer/osef-posix/osef-log/Debug -I../../osef-netbuffer/NetBuffer -I../../osef-netbuffer/osef-posix/Socket -I../../osef-netbuffer/osef-posix/Mutex -I../../osef-netbuffer/osef-posix/Thread -I../../osef-netbuffer/osef-posix/MsgQ -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/8b36c9fe/ModbusClient.o ../../Modbus/ModbusClient.cpp

${OBJECTDIR}/_ext/8b36c9fe/ModbusRegContainer.o: ../../Modbus/ModbusRegContainer.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/8b36c9fe
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../osef-netbuffer/osef-posix/osef-log/Debug -I../../osef-netbuffer/NetBuffer -I../../osef-netbuffer/osef-posix/Socket -I../../osef-netbuffer/osef-posix/Mutex -I../../osef-netbuffer/osef-posix/Thread -I../../osef-netbuffer/osef-posix/MsgQ -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/8b36c9fe/ModbusRegContainer.o ../../Modbus/ModbusRegContainer.cpp

${OBJECTDIR}/_ext/8b36c9fe/ModbusRequestReadCoils.o: ../../Modbus/ModbusRequestReadCoils.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/8b36c9fe
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../osef-netbuffer/osef-posix/osef-log/Debug -I../../osef-netbuffer/NetBuffer -I../../osef-netbuffer/osef-posix/Socket -I../../osef-netbuffer/osef-posix/Mutex -I../../osef-netbuffer/osef-posix/Thread -I../../osef-netbuffer/osef-posix/MsgQ -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/8b36c9fe/ModbusRequestReadCoils.o ../../Modbus/ModbusRequestReadCoils.cpp

${OBJECTDIR}/_ext/8b36c9fe/ModbusRequestReadHoldingReg.o: ../../Modbus/ModbusRequestReadHoldingReg.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/8b36c9fe
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../osef-netbuffer/osef-posix/osef-log/Debug -I../../osef-netbuffer/NetBuffer -I../../osef-netbuffer/osef-posix/Socket -I../../osef-netbuffer/osef-posix/Mutex -I../../osef-netbuffer/osef-posix/Thread -I../../osef-netbuffer/osef-posix/MsgQ -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/8b36c9fe/ModbusRequestReadHoldingReg.o ../../Modbus/ModbusRequestReadHoldingReg.cpp

${OBJECTDIR}/_ext/8b36c9fe/ModbusRequestReadInputReg.o: ../../Modbus/ModbusRequestReadInputReg.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/8b36c9fe
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../osef-netbuffer/osef-posix/osef-log/Debug -I../../osef-netbuffer/NetBuffer -I../../osef-netbuffer/osef-posix/Socket -I../../osef-netbuffer/osef-posix/Mutex -I../../osef-netbuffer/osef-posix/Thread -I../../osef-netbuffer/osef-posix/MsgQ -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/8b36c9fe/ModbusRequestReadInputReg.o ../../Modbus/ModbusRequestReadInputReg.cpp

${OBJECTDIR}/_ext/8b36c9fe/ModbusRequestWriteCoils.o: ../../Modbus/ModbusRequestWriteCoils.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/8b36c9fe
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../osef-netbuffer/osef-posix/osef-log/Debug -I../../osef-netbuffer/NetBuffer -I../../osef-netbuffer/osef-posix/Socket -I../../osef-netbuffer/osef-posix/Mutex -I../../osef-netbuffer/osef-posix/Thread -I../../osef-netbuffer/osef-posix/MsgQ -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/8b36c9fe/ModbusRequestWriteCoils.o ../../Modbus/ModbusRequestWriteCoils.cpp

${OBJECTDIR}/_ext/8b36c9fe/ModbusRequestWriteHoldingReg.o: ../../Modbus/ModbusRequestWriteHoldingReg.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/8b36c9fe
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../osef-netbuffer/osef-posix/osef-log/Debug -I../../osef-netbuffer/NetBuffer -I../../osef-netbuffer/osef-posix/Socket -I../../osef-netbuffer/osef-posix/Mutex -I../../osef-netbuffer/osef-posix/Thread -I../../osef-netbuffer/osef-posix/MsgQ -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/8b36c9fe/ModbusRequestWriteHoldingReg.o ../../Modbus/ModbusRequestWriteHoldingReg.cpp

${OBJECTDIR}/_ext/8b36c9fe/ModbusResponseReadCoils.o: ../../Modbus/ModbusResponseReadCoils.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/8b36c9fe
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../osef-netbuffer/osef-posix/osef-log/Debug -I../../osef-netbuffer/NetBuffer -I../../osef-netbuffer/osef-posix/Socket -I../../osef-netbuffer/osef-posix/Mutex -I../../osef-netbuffer/osef-posix/Thread -I../../osef-netbuffer/osef-posix/MsgQ -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/8b36c9fe/ModbusResponseReadCoils.o ../../Modbus/ModbusResponseReadCoils.cpp

${OBJECTDIR}/_ext/8b36c9fe/ModbusResponseReadHoldingReg.o: ../../Modbus/ModbusResponseReadHoldingReg.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/8b36c9fe
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../osef-netbuffer/osef-posix/osef-log/Debug -I../../osef-netbuffer/NetBuffer -I../../osef-netbuffer/osef-posix/Socket -I../../osef-netbuffer/osef-posix/Mutex -I../../osef-netbuffer/osef-posix/Thread -I../../osef-netbuffer/osef-posix/MsgQ -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/8b36c9fe/ModbusResponseReadHoldingReg.o ../../Modbus/ModbusResponseReadHoldingReg.cpp

${OBJECTDIR}/_ext/8b36c9fe/ModbusResponseReadInputReg.o: ../../Modbus/ModbusResponseReadInputReg.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/8b36c9fe
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../osef-netbuffer/osef-posix/osef-log/Debug -I../../osef-netbuffer/NetBuffer -I../../osef-netbuffer/osef-posix/Socket -I../../osef-netbuffer/osef-posix/Mutex -I../../osef-netbuffer/osef-posix/Thread -I../../osef-netbuffer/osef-posix/MsgQ -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/8b36c9fe/ModbusResponseReadInputReg.o ../../Modbus/ModbusResponseReadInputReg.cpp

${OBJECTDIR}/_ext/8b36c9fe/ModbusResponseWriteCoils.o: ../../Modbus/ModbusResponseWriteCoils.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/8b36c9fe
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../osef-netbuffer/osef-posix/osef-log/Debug -I../../osef-netbuffer/NetBuffer -I../../osef-netbuffer/osef-posix/Socket -I../../osef-netbuffer/osef-posix/Mutex -I../../osef-netbuffer/osef-posix/Thread -I../../osef-netbuffer/osef-posix/MsgQ -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/8b36c9fe/ModbusResponseWriteCoils.o ../../Modbus/ModbusResponseWriteCoils.cpp

${OBJECTDIR}/_ext/8b36c9fe/ModbusResponseWriteHoldingReg.o: ../../Modbus/ModbusResponseWriteHoldingReg.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/8b36c9fe
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../osef-netbuffer/osef-posix/osef-log/Debug -I../../osef-netbuffer/NetBuffer -I../../osef-netbuffer/osef-posix/Socket -I../../osef-netbuffer/osef-posix/Mutex -I../../osef-netbuffer/osef-posix/Thread -I../../osef-netbuffer/osef-posix/MsgQ -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/8b36c9fe/ModbusResponseWriteHoldingReg.o ../../Modbus/ModbusResponseWriteHoldingReg.cpp

${OBJECTDIR}/_ext/8b36c9fe/ModbusServer.o: ../../Modbus/ModbusServer.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/8b36c9fe
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../osef-netbuffer/osef-posix/osef-log/Debug -I../../osef-netbuffer/NetBuffer -I../../osef-netbuffer/osef-posix/Socket -I../../osef-netbuffer/osef-posix/Mutex -I../../osef-netbuffer/osef-posix/Thread -I../../osef-netbuffer/osef-posix/MsgQ -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/8b36c9fe/ModbusServer.o ../../Modbus/ModbusServer.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
